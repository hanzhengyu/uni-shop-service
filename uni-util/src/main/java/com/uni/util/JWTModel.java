package com.uni.util;

import com.uni.entity.ShopUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JWTModel {

    /**
     * 用户唯一id
     */
    private Long id;
    /**
     * 用户名 唯一
     */
    private String userName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 角色 0-管理员 1-普通用户 2-卖家
     */
    private Integer role;
    /**
     * 头像图片地址
     */
    private String avatar;
    /**
     * 积分
     */
    private Integer integral;
    /**
     * 收货地址,JSON格式
     */
    private String address;

    /**
     * 用户令牌
     */
    private String jwt;

    public JWTModel(ShopUser user, String jwt) {
        this.id = user.getId();
        this.userName = user.getUserName();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.role = user.getRole();
        this.avatar = user.getAvatar();
        this.integral = user.getIntegral();
        this.address = user.getAddress();
        this.jwt = jwt;
    }
}
