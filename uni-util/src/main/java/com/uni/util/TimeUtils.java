package com.uni.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TimeUtils {

    /**
     * 获取当前时间Date对象
     * @return date
     * @throws ParseException
     */
    public static Date concurrentDate() throws ParseException {
        String now = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
        return strToDate(now);
    }

    /**
     * string转date
     * @param str 时间字符串
     * @return date
     * @throws ParseException
     */
    public static Date strToDate(String str) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = format.parse(str);
        return date;
    }
}
