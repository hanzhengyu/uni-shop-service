package com.uni.controller;


import com.uni.entity.Dto;
import com.uni.entity.ShopImages;
import com.uni.entity.ShopProduct;
import com.uni.service.ShopImagesService;
import com.uni.service.ShopProductService;
import com.uni.util.DtoUtil;
import com.uni.util.FastDFSClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/img")
public class ImagesController {

    @Autowired
    private ShopImagesService shopImagesService;

    @Autowired
    private ShopProductService service;

    /**
     * 获取type为0的轮播图列表
     * @return
     */
    @GetMapping("/carousel")
    public Dto carousel(){

        return DtoUtil.returnDataSuccess(
                shopImagesService.queryAll(new ShopImages(0))
        );
    }

    /**
     * 分页 获取图片列表
     * @param page 页码
     * @param limit 每页条数
     * @return
     */
    @GetMapping("/getAll")
    public Dto queryAll(Integer page, Integer limit){

        List<ShopImages> shopImages = shopImagesService.queryAllByLimit((page - 1) * limit, limit);

        return DtoUtil.returnPageSuccess(shopImages, shopImagesService.getCount());
    }

    /**
     * 新增图片
     * @param shopImages
     * @return
     */
    @PostMapping("/add")
    public Dto addImage( ShopImages shopImages){
        if (shopImages != null){
            shopImagesService.insert(shopImages);
        }else {
            return DtoUtil.returnFail("参数为空", "400");
        }
        return DtoUtil.returnSuccess("插入成功");
    }

    /**
     * 删除/批量删除图片
     * @param ids id列表
     * @return
     */
    @PostMapping("/delete")
    public Dto deleteImage(@RequestParam("ids") List<Integer> ids){
        boolean b = shopImagesService.deleteById(ids);
        if (!b)
            return DtoUtil.returnFail("删除失败", "500");
        else
            return DtoUtil.returnSuccess("删除成功");
    }

    /**
     * 根据id获取图片
     * @param id
     * @return
     */
    @GetMapping("get/{id}")
    public Dto getById(@PathVariable("id") Integer id) {

        if (id == 0)
            return DtoUtil.returnFail("id不能为空", "400");

        return DtoUtil.returnDataSuccess(shopImagesService.queryById(id));
    }

    /**
     * 编辑图片信息
     * @param shopImages
     * @return
     */
    @PostMapping("/edit")
    public Dto editImage(ShopImages shopImages){
        if (shopImages.getId() == null)
            return DtoUtil.returnFail("参数不能为空", "400");

        try {
            shopImagesService.update(shopImages);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return DtoUtil.returnSuccess("修改成功");
    }

    /**
     * 图片上传
     * @param file
     * @param url 如果有旧图片需要替换
     * @return
     */
    @PostMapping("uploadImg")
    public Dto uploadImg(MultipartFile file,String url){
        if (file.isEmpty()) {
            return DtoUtil.returnFail("文件夹为空","500");
        }
        if(!StringUtils.isEmpty(url)){
            FastDFSClientUtils.delete(url.split("/")[0],url);
        }
        try {
            //图片二进制流，获取图片名称
            return DtoUtil.returnSuccess("上传成功",
                    FastDFSClientUtils.upload(file.getBytes(),file.getOriginalFilename())
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DtoUtil.returnFail("上传失败，请联系管理员","500");
    }


    /*根据分类的id查询商品表中的数据*/
    @RequestMapping("selectProduct")
    public  List<ShopProduct> selectProduct(int categoryId){

         return service.selectShopProduct(categoryId);

    }


}
