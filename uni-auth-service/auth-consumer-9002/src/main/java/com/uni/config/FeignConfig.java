package com.uni.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    public Logger.Level feignLoggerLevel() {
        return  Logger.Level.BASIC; //记录请求方法,URL, 响应状态码及执行时间
    }
}

