package com.uni.controller;

import com.uni.entity.Dto;
import com.uni.entity.ShopUser;
import com.uni.service.AuthService;
import com.uni.util.DtoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/auth/consumer")
public class AuthController {

    @Autowired
    private AuthService authService;

    /**
     * 用户登录
     * @param user 实例对象
     * @return 返回用户信息和token
     */
    @PostMapping("/login")
    public Dto doLogin(@RequestBody ShopUser user){

        if (StringUtils.isEmpty(user.getPassword()))
            return DtoUtil.returnFail("密码为空", "405");
        if (StringUtils.isEmpty(user.getUserName()) && StringUtils.isEmpty(user.getPhone()))
            return DtoUtil.returnFail("账号为空", "405");

        return authService.doLogin(user);
    }

    /**
     * 用户登录
     * @param user 实例对象
     * @return 返回用户信息和token
     */
    @PostMapping("/reg")
    public Dto register(@RequestBody ShopUser user){

        if (StringUtils.isEmpty(user.getPassword()))
            return DtoUtil.returnFail("密码为空", "405");
        if (StringUtils.isEmpty(user.getUserName()) && StringUtils.isEmpty(user.getPhone()))
            return DtoUtil.returnFail("账号为空", "405");

        return authService.register(user);
    }

    @PostMapping("/checkName")
    public Dto checkName(@RequestParam("userName") String userName){
        if (StringUtils.isEmpty(userName))
            return DtoUtil.returnFail("参数用户名为空", "405");

        return authService.checkName(userName);
    }


}
