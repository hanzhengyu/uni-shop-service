package com.uni.service;

import com.uni.entity.Dto;
import com.uni.entity.ShopUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@FeignClient(value = "auth-provider")
public interface AuthService {

    @PostMapping("/auth/login")
    Dto doLogin(@RequestBody ShopUser user);

    @PostMapping("/auth/reg")
    Dto register(@RequestBody ShopUser user);

    @GetMapping("/user/getPage")
    Dto userList(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit);

    @GetMapping("/user/get/{id}")
    Dto getUserById(@PathVariable("id") Long id);

    @GetMapping("/user/delete")
    Dto deleteUserById(@RequestParam("ids") List<Long> ids);

    @PostMapping("/user/update")
    Dto update(@RequestBody ShopUser user);

    @PostMapping("/auth/checkName")
    Dto checkName(@RequestParam("userName") String userName);
}
