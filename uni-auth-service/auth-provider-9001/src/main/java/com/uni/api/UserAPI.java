package com.uni.api;

import com.uni.entity.Dto;
import com.uni.entity.ShopUser;
import com.uni.service.ShopUserService;
import com.uni.util.DtoUtil;
import com.uni.util.FastDFSClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserAPI {

    @Autowired
    private ShopUserService shopUserService;

    /**
     * 分页查询
     * @param page 页码
     * @param limit 每页条数
     * @return page
     */
    @GetMapping("/getPage")
    public Dto userList(Integer page, Integer limit){
        if (page <= 0)
            page = 1;

        List<ShopUser> userList =
                shopUserService.queryAllByLimit((page - 1) * limit, limit);
        return DtoUtil.returnPageSuccess(userList, shopUserService.getCount());
    }

    /**
     * 根据id查询
     * @param id
     * @return 用户信息
     */
    @GetMapping("/get/{id}")
    public Dto getUserById(@PathVariable("id") Long id){
        if (id == null)
            return DtoUtil.returnFail("参数id为空", "405");

        ShopUser result = shopUserService.queryById(id);
        if (ObjectUtils.isEmpty(result))
            return DtoUtil.returnFail("该用户不存在", "404");
        return DtoUtil.returnDataSuccess(result);
    }

    /**
     * 删除用户
     * @param ids id列表
     * @return 响应
     */
    @GetMapping("/delete")
    public Dto deleteUserById(@RequestParam("ids") List<Long> ids){
        if (ids.size() <= 0)
            return DtoUtil.returnFail("参数id为空", "405");
        if (!shopUserService.deleteById(ids))
            return DtoUtil.returnFail("删除失败", "500");

        return DtoUtil.returnSuccess("删除成功");
    }



    /**
     * 更新用户信息
     * @param user 实例对象
     * @return 响应
     */
    @PostMapping("/update")
    public Dto update(@RequestBody ShopUser user){
        ShopUser result = null;

        try {
            if (ObjectUtils.isNotEmpty(user)){
                result = shopUserService.update(user);
            }
        } catch (Exception e) {
            log.error("更新用户信息时异常: ", e);
            return DtoUtil.returnFail("更新失败", "500");
        }

        return DtoUtil.returnSuccess("更新成功", result);
    }

    /**
     * 用户头像上传
     * @param file
     * @param url 如果有旧图片需要替换
     * @return 图片地址
     */
    @PostMapping("/uploadAvatar")
    public Dto uploadImg(MultipartFile file, String url){
        if (file.isEmpty()) {
            return DtoUtil.returnFail("文件为空","500");
        }
        if(!StringUtils.isEmpty(url)){
            FastDFSClientUtils.delete(url.split("/")[0],url);
        }
        try {
            //上传图片并返回url
            return DtoUtil.returnSuccess("上传成功",
                    FastDFSClientUtils.upload(file.getBytes(),file.getOriginalFilename())
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DtoUtil.returnFail("上传失败，请联系管理员","500");
    }

    /**
     * 用户注销
     * @param request 请求体
     * @return 响应
     */
    @GetMapping("/logout")
    public Dto logout(HttpServletRequest request){
        return DtoUtil.returnSuccess("注销成功");
    }

}
