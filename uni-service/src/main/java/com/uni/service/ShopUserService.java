package com.uni.service;

import com.uni.entity.ShopUser;

import java.text.ParseException;
import java.util.List;

/**
 * (ShopUser)表服务接口
 *
 * @author makejava
 * @since 2020-06-27 16:02:34
 */
public interface ShopUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ShopUser queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ShopUser> queryAllByLimit(int offset, int limit);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param shopUser 实例对象
     * @return 对象列表
     */
    List<ShopUser> queryAll(ShopUser shopUser);

    /**
     * 新增数据
     *
     * @param shopUser 实例对象
     * @return 实例对象
     */
    ShopUser insert(ShopUser shopUser);

    /**
     * 修改数据
     *
     * @param shopUser 实例对象
     * @return 实例对象
     */
    ShopUser update(ShopUser shopUser) throws Exception;

    /**
     * 通过主键删除数据
     *
     * @param ids 主键列表
     * @return 是否成功
     */
    boolean deleteById(List<Long> ids);

    /**
     * 用户登录查询
     * @param user 实例对象
     * @return
     */
    ShopUser login(ShopUser user);

    /**
     * 查询数据条数
     * @return int
     */
    int getCount();

    /**
     * 根据用户名查询
     * @param userName
     * @return
     */
    ShopUser getByName(String userName);

}