package com.uni.service.impl;

import com.uni.entity.ShopUser;
import com.uni.dao.ShopUserDao;
import com.uni.service.ShopUserService;
import com.uni.util.IdGeneratorSnowflake;
import com.uni.util.TimeUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * (ShopUser)表服务实现类
 *
 * @author makejava
 * @since 2020-06-27 16:02:34
 */
@Service("shopUserService")
public class ShopUserServiceImpl implements ShopUserService {
    @Resource
    private ShopUserDao shopUserDao;

    @Resource
    private IdGeneratorSnowflake idGeneratorSnowflake;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ShopUser queryById(Long id) {
        return this.shopUserDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<ShopUser> queryAllByLimit(int offset, int limit) {
        return this.shopUserDao.queryAllByLimit(offset, limit);
    }

    @Override
    public List<ShopUser> queryAll(ShopUser shopUser) {
        return this.shopUserDao.queryAll(shopUser);
    }

    /**
     * 新增数据
     *
     * @param shopUser 实例对象
     * @return 实例对象
     */
    @Override
    public ShopUser insert(ShopUser shopUser) {
        shopUser.setId(idGeneratorSnowflake.snowflakeId());
        this.shopUserDao.insert(shopUser);
        return shopUser;
    }

    /**
     * 修改数据
     *
     * @param shopUser 实例对象
     * @return 实例对象
     */
    @Override
    public ShopUser update(ShopUser shopUser) throws Exception {
        //设置修改时间
        shopUser.setUpdateTime(TimeUtils.concurrentDate());
        this.shopUserDao.update(shopUser);
        return this.queryById(shopUser.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键列表
     * @return 是否成功
     */
    @Override
    public boolean deleteById(List<Long> ids) {
        return this.shopUserDao.deleteById(ids) > 0;
    }

    @Override
    public ShopUser login(ShopUser user) {
        return this.shopUserDao.login(user);
    }

    @Override
    public int getCount() {
        return this.shopUserDao.getCount();
    }

    @Override
    public ShopUser getByName(String userName) {
        return this.shopUserDao.getByName(userName);
    }
}