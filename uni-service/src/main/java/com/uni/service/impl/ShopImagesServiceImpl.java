package com.uni.service.impl;


import com.uni.dao.ShopImagesDao;
import com.uni.entity.ShopImages;
import com.uni.service.ShopImagesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ShopImages)表服务实现类
 *
 * @author makejava
 * @since 2020-06-10 13:23:35
 */
@Service("shopImagesService")
public class ShopImagesServiceImpl implements ShopImagesService {
    @Resource
    private ShopImagesDao shopImagesDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ShopImages queryById(Integer id) {
        return this.shopImagesDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<ShopImages> queryAllByLimit(int offset, int limit) {
        return this.shopImagesDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param shopImages 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(ShopImages shopImages) {
        this.shopImagesDao.insert(shopImages);
        return 1;
    }

    /**
     * 修改数据
     *
     * @param shopImages 实例对象
     * @return 实例对象
     */
    @Override
    public ShopImages update(ShopImages shopImages) {
        this.shopImagesDao.update(shopImages);
        return this.queryById(shopImages.getId());
    }

    @Override
    public List<ShopImages> queryAll(ShopImages shopImages) {
        return this.shopImagesDao.queryAll(shopImages);
    }

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(List<Integer> ids) {
        return this.shopImagesDao.deleteById(ids) == ids.size();
    }

    /**
     * 获取数据条数,用于分页
     * @return
     */
    @Override
    public Integer getCount() {
        return shopImagesDao.getCount();
    }
}