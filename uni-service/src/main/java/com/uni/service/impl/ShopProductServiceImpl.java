package com.uni.service.impl;

import com.uni.dao.ShopProductDao;
import com.uni.entity.ShopProduct;
import com.uni.service.ShopProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ShopProductService")
public class ShopProductServiceImpl implements ShopProductService {

    /*注册bean失败,在注解上加入required = false*/
    @Autowired(required = false)
    private ShopProductDao shopProductDao;

    @Override
    public List<ShopProduct> selectShopProduct(int categoryId) {
        return shopProductDao.selectShopProduct(categoryId);
    }
}
