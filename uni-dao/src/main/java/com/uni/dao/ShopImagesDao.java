package com.uni.dao;

import com.uni.entity.ShopImages;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (ShopImages)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-10 13:20:06
 */
@Mapper
public interface ShopImagesDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ShopImages queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ShopImages> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param shopImages 实例对象
     * @return 对象列表
     */
    List<ShopImages> queryAll(ShopImages shopImages);

    /**
     * 新增数据
     *
     * @param shopImages 实例对象
     * @return 影响行数
     */
    int insert(ShopImages shopImages);

    /**
     * 修改数据
     *
     * @param shopImages 实例对象
     * @return 影响行数
     */
    int update(ShopImages shopImages);

    /**
     * 通过主键删除数据
     *
     * @param ids 主键
     * @return 影响行数
     */
    int deleteById(@Param("ids") List<Integer> ids);

    Integer getCount();




}
