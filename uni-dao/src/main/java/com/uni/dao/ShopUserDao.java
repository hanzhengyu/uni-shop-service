package com.uni.dao;

import com.uni.entity.ShopUser;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (ShopUser)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-27 16:01:27
 */
public interface ShopUserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ShopUser queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ShopUser> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param shopUser 实例对象
     * @return 对象列表
     */
    List<ShopUser> queryAll(ShopUser shopUser);

    /**
     * 用户登录查询
     * @param user 实例对象
     * @return
     */
    ShopUser login(ShopUser user);

    /**
     * 新增数据
     *
     * @param shopUser 实例对象
     * @return 影响行数
     */
    int insert(ShopUser shopUser);

    /**
     * 修改数据
     *
     * @param shopUser 实例对象
     * @return 影响行数
     */
    int update(ShopUser shopUser);

    /**
     * 通过主键删除数据
     *
     * @param ids 主键列表
     * @return 影响行数
     */
    int deleteById(List<Long> ids);

    /**
     * 查询数据条数
     * @return int
     */
    int getCount();

    /**
     * 根据用户名查询
     * @param userName
     * @return
     */
    ShopUser getByName(String userName);

}