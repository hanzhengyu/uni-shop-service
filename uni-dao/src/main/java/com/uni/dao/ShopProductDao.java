package com.uni.dao;

import com.uni.entity.ShopProduct;

import java.util.List;

public interface ShopProductDao {

       /*根据分类id查询商品表*/
       public List<ShopProduct> selectShopProduct(int categoryId);



}
