package com.uni.entity;

import java.io.Serializable;

/**
 * (ShopImages)实体类
 *
 * @author makejava
 * @since 2020-06-09 14:47:25
 */
public class ShopImages implements Serializable {
    private static final long serialVersionUID = 145123044009204367L;
    /**
    * 主键
    */
    private Integer id;
    /**
    * 图片地址

    */
    private String src;
    /**
    * 背景色
    */
    private String background;
    /**
    * 图片类型(0:轮播图, 1:秒杀, 2:团购, 3:分类, 4: 猜你喜欢, 5:其他)
    */
    private Integer type;
    /**
    * 目标地址
    */
    private String url;

    public ShopImages() {
    }

    public ShopImages(Integer type) {
        this.type = type;
    }

    public ShopImages(String src) {
        this.src = src;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ShopImages{" +
                "id=" + id +
                ", src='" + src + '\'' +
                ", background='" + background + '\'' +
                ", type=" + type +
                ", url='" + url + '\'' +
                '}';
    }
}