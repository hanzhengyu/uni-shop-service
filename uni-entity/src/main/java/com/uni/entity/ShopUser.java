package com.uni.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (ShopUser)实体类
 *
 * @author makejava
 * @since 2020-06-27 15:57:35
 */
public class ShopUser implements Serializable {
    private static final long serialVersionUID = -71487382236448172L;
    /**
    * 用户唯一id
    */
    private Long id;
    /**
    * 用户名 唯一 
    */
    private String userName;
    /**
    * 用户密码 非空
    */
    private String password;
    /**
    * 邮箱
    */
    private String email;
    /**
    * 电话
    */
    private String phone;
    /**
    * 角色 0-管理员 1-普通用户 2-卖家
    */
    private Integer role;
    /**
    * 头像图片地址
    */
    private String avatar;
    /**
    * 积分
    */
    private Integer integral;
    /**
    * 收货地址,JSON格式
    */
    private String address;
    /**
    * 创建时间
    */
    private Date createTime;
    /**
    * 更新时间
    */
    private Date updateTime;

    public ShopUser() {
    }

    public ShopUser(String userName) {
        this.userName = userName;
    }

    public ShopUser(Long id, String userName, Integer role) {
        this.id = id;
        this.userName = userName;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}