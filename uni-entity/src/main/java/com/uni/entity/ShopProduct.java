package com.uni.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * (ShopProduct)实体类
 *
 * @author makejava
 * @since 2020-06-26 12:20:46
 */
public class ShopProduct implements Serializable {
    private static final long serialVersionUID = 733077625207675978L;
    /**
    * 商品的id
    */
    private Integer id;
    /**
    * 分类id,分类字典表的主键
    */
    private Integer categoryId;
    /**
    * 商品名称
    */
    private String name;
    /**
    * 商品主图,图片片表id
    */
    private String mainImage;
    /**
    * 价格,单位-元保留两位小数
    */
    private Double price;
    /**
    * 商品主图,图片片表id
    */
    private Integer stock;
    /**
    * 商品状态, 0-在售, 1-下架
    */
    private Integer status;
    /**
    * 创建时间
    */
    private Date createTime;
    /**
    * 更新时间
    */
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}